import logging

import tweepy
from tweepy.error import TweepError

from decorators import retry_if_error, decorate_all_methods

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Twitter():
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        logger.debug('Instantiating Twitter')
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.current_pages = None

    @property
    def api(self):
        logger.debug('Retrieving API')
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_token, self.access_token_secret)
        return tweepy.API(auth)

    def pagination(self, method, param, current=None):
        try:
            pages = tweepy.Cursor(method, param).pages()
            self.current_pages = pages or None
            if current:
                for page in current:
                    logger.debug('\n\nGetting results from page number: %s\n' % current.index)
                    self.current_pages = current
                    yield page
            else:
                for page in pages:
                    logger.debug('\n\nGetting results from page number: %s\n' % pages.index)
                    self.current_pages = pages
                    yield page
        except TweepError as ex:
            if '429' in ex.reason:
                logger.error('You reached Twitter API limit')
                logger.debug('\n\nCalling method with object starting at page: %s\n' % self.current_pages.index)
                self.pagination(method, param, current=self.current_pages)
            else:
                logger.error('TweepError: %s' % str(ex))
        except Exception as ex:
            logger.error('Error: %s' % str(ex))


@decorate_all_methods(retry_if_error)
class User():
    def __init__(self, twitter, username):
        logger.debug('Instantiating User: %s' % username)
        self.twitter = twitter
        self.api = twitter.api
        self.username = username
        self.user = self.api.get_user(self.username)

    def info(self):
        logger.debug('Getting user info')
        return {
            'id': self.id(),
            'name': self.name(),
            'username': self.username,
            'created_at': self.created_at(),
            'profile_image': self.profile_image(),
            'location': self.location(),
            'language': self.language(),
            'bio': self.bio(),
            'total_following': self.total_following(),
            'total_followers': self.total_followers(),
            'total_tweets': self.total_tweets()
        }

    def id(self):
        return self.user.id

    def name(self):
        return self.user.name

    def created_at(self):
        return self.user.created_at

    def profile_image(self):
        return self.user.profile_image_url

    def location(self):
        return self.user.location or ''

    def language(self):
        return self.user.lang

    def bio(self):
        return self.user.description or ''

    def total_following(self):
        return self.user.friends_count

    def total_followers(self):
        return self.user.followers_count

    def total_tweets(self):
        return self.user.statuses_count

    def following(self):
        for users in self.twitter.pagination(method=self.api.friends, param=self.username):
            for user in users:
                yield user.screen_name

    def followers(self):
        for users in self.twitter.pagination(method=self.api.followers, param=self.username):
            for user in users:
                yield user.screen_name

    def tweets(self):
        for tweets in self.twitter.pagination(method=self.api.user_timeline, param=self.username):
            for tweet in tweets:
                yield tweet

    def __str__(self):
        return self.username


@decorate_all_methods(retry_if_error)
class Tweet():
    def __init__(self, twitter, tweet):
        self.twitter = twitter
        self.api = twitter.api
        self.tweet = tweet
        self.tweet_id = self.id()

    def __get_username_by_id(self, user_id):
        user_id = str(user_id)
        return self.api.get_user(user_id=user_id).screen_name

    def id(self):
        return self.tweet.id

    def info(self):
        return {
            'id': self.tweet_id,
            'text': self.text(),
            'tweet_type': self.type(),
            'author': self.author(),
            'created_at': self.created_at(),
            'source': self.source(),
            'language': self.language(),
            'geo': str(self.geo()),
            'coordinates': str(self.coordinates()),
            'place': str(self.place()),
            'hashtags': [hashtag for hashtag in self.hashtags()],
            'total_retweet': self.total_retweet(),
            'total_favorite': self.total_favorite()
        }

    def text(self):
        return self.tweet.text

    def type(self):
        if hasattr(self.tweet, 'retweeted_status'):
            return 'retweet'
        elif self.tweet.in_reply_to_status_id:
            return 'reply'
        else:
            return 'tweet'

    def author(self):
        return self.tweet.author.screen_name

    def created_at(self):
        return self.tweet.created_at

    def source(self):
        return self.tweet.source

    def language(self):
        return self.tweet.lang

    def geo(self):
        return self.tweet.geo or ''

    def coordinates(self):
        return self.tweet.coordinates or ''

    def place(self):
        return self.tweet.place or ''

    def hashtags(self):
        for hashtag in self.tweet.entities['hashtags']:
            yield hashtag['text']

    def total_retweet(self):
        return self.tweet.retweet_count

    def total_favorite(self):
        return self.tweet.favorite_count

    def user_mentions(self):
        for mention in self.tweet.entities['user_mentions']:
            yield self.__get_username_by_id(user_id=mention['id'])

    def replies(self):
        # There are no support to get tweet replies: http://bit.ly/1UzyVCA
        # TODO: solve this problem ;)
        pass

    def retweets(self):
        # Twitter returns only 100 most recent retweets: http://bit.ly/1Eh5Tw4
        for tweet in self.api.retweets(id=self.tweet_id, count=100):
            yield {'tweet_id': tweet.id, 'who_retweeted': tweet.user.screen_name}

    def __str__(self):
        return str(self.tweet_id)


@decorate_all_methods(retry_if_error)
class Trends():
    def __init__(self, twitter):
        self.twitter = twitter
        self.api = twitter.api

    def country_info(self):
        results = self.api.trends_available(_woeid=1)
        info = []
        for result in results:
            url, code = result['url'].split('place/')
            countryCode = result['countryCode']
            info.append({'countryCode': countryCode, 'woeid': code})
        return set(info)

    def trends(self, woeid):
        for info in self.api.trends_place(id=woeid):
            for trend in info['trends']:
                yield trend['name']

    def world_trends(self):
        return [trend for trend in self.trends(woeid=1)]

    def all_country_trends(self):
        for info in self.country_info():
            yield self.trends(info['woeid'])

    def brazilian_cities_trends(self):
        pass


@decorate_all_methods(retry_if_error)
class Search():
    def __init__(self, twitter):
        self.twitter = twitter
        self.api = twitter.api

    def search_tweet_by_keyword(self, query):
        """
        Search tweets by hashtags or keywords
        :param: query: hashtag or keyword
        :return: tweet object
        """
        for tweets in self.twitter.pagination(method=self.api.search, param=query):
            for tweet in tweets:
                yield tweet
