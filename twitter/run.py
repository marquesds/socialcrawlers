from twitter import *
import settings
import logging
from decorators import retry_if_error
from pymongo import MongoClient

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

TWITTER = Twitter(settings.CONSUMER_KEY1, settings.CONSUMER_SECRET1, settings.ACCESS_TOKEN1,
                  settings.ACCESS_TOKEN_SECRET1)

try:
    MONGODB = client = MongoClient(settings.DATABASE_URL, settings.DATABASE_PORT)
except Exception as ex:
    logger.error("Error: Could't connect to mongodb. Caused by: %s" % str(ex))


@retry_if_error
def save(username):
    logger.debug('Saving user and tweets')
    user = User(TWITTER, username)

    collections = MONGODB.twitter
    collection_name = username.replace('.', '').replace('_', '')
    collection = collections[collection_name]

    # Work only in new connections
    if not collection_name in collections.collection_names():
        user_following = [following for following in user.following()]
        user_followers = [follower for follower in user.followers()]
        tweets = [tweet for tweet in user.tweets()]

        user_all_data = {
            'type': 'about',
            'info': user.info(),
            'following': user_following,
            'followers': user_followers,
            'tweets': [tweet.id for tweet in tweets]
        }

        logger.debug('Saving user: %s' % username)
        collection.insert(user_all_data)

        for tweet in tweets:
            save_tweet(collection_name, tweet)


@retry_if_error
def save_tweet(collection_name, tweet_obj, **kwargs):
    collections = MONGODB.twitter
    collection = collections[collection_name]

    tweet = Tweet(TWITTER, tweet_obj)
    user_mentions = [mention for mention in tweet.user_mentions()]
    retweets = [retweet for retweet in tweet.retweets()]
    tweet_all_data = {
        'type': 'tweet',
        'info': tweet.info(),
        'mentions': user_mentions,
        'retweets': retweets
    }

    for key, value in kwargs.items():
        tweet_all_data[key] = value

    logger.debug('Saving tweet: %s' % tweet)
    collection.insert(tweet_all_data)


@retry_if_error
def save_trends():
    logger.debug('Saving trends')
    trends = Trends(TWITTER)
    world_trends = MONGODB.twitter['world_trends']
    all_country_trends = MONGODB.twitter['all_country_trends']
    brazilian_cities_trends = MONGODB.twitter['brazilian_cities_trends']

    for trend in trends.world_trends():
        logger.debug('Saving trend: %s' % trend)
        world_trends.insert(trend)

    for trend in trends.all_country_trends():
        logger.debug('Saving trend: %s' % trend)
        all_country_trends.insert(trend)

    for trend in trends.brazilian_cities_trends():
        logger.debug('Saving trend: %s' % trend)
        brazilian_cities_trends.insert(trend)


@retry_if_error
def save_keyword(collection_name, word):
    logger.debug('Saving searched tweets')
    search_type = 'hashtag' if word.startswith('#') else 'keyword'
    search = Search(TWITTER)
    tweets = [tweet for tweet in search.search_tweet_by_keyword(query=word)]
    for tweet in tweets:
        save_tweet(collection_name, tweet, search_type=search_type, word=word)


if __name__ == '__main__':
    try:
        logger.debug('Starting program...')
        with open('brands.txt') as file:
            for brand in file:
                save(brand.replace('\n', ''))
        logger.debug('Program has finished')
    except Exception as ex:
        logger.error('Error: %s' % str(ex))
