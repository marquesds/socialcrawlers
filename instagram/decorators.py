import logging
from functools import wraps
import time
from instagram.bind import InstagramAPIError

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def retry_if_error(func):
    @wraps(func)
    def retry(*args, **kwargs):
        try:
            logger.debug('Getting data from function: %s' % func.__name__)
            return func(*args, **kwargs)
        except InstagramAPIError as ex:
            if '429' in str(ex.status_code):
                # More than one hour
                logger.error('You reached Instagram API limit. Waiting...')
                time.sleep(60)
                retry(*args, **kwargs)
            else:
                logger.error('TweepError: %s' % str(ex))
        except Exception as ex:
            logger.error('\n\nError in method: %s\n' % func.__name__)
            logger.error('Unknown error: %s\n' % str(ex))
            return "Error: Could't retrieve data. Caused by: %s" % str(ex)

    return retry


def decorate_all_methods(decorator):
    '''
    Decorate all methods in a class with decorator passed by param
    '''

    def decorate(cls):
        for attr in cls.__dict__:
            if callable(getattr(cls, attr)):
                # Ignore class methods like __init__
                if not attr.startswith('__'):
                    setattr(cls, attr, decorator(getattr(cls, attr)))
        return cls

    return decorate
