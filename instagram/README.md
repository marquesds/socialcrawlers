# Instagram
Bot que pega dados de contas do instagram e salva no MongoDB

# Instação das dependências
`pip install -r requirements.txt`

# Configuração
Salve a lista das marcas que serão baixadas em `brands.txt`. O arquivo tem um modelo de como os dados devem ser inseridos lá.

# Execução do código
Para executar o código basta rodar o comando abaixo:
<br>
`python run.py`

# Suporte
Este programa suporta as versões 2 e 3 do Python.
