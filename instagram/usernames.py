import csv


def segment():
    with open('planilha.csv', newline='') as csvfile:
        usernames = []
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            instagram = row[12].strip()
            if instagram:
                usernames.append(instagram)

    usernames.remove('instagram')
    return usernames


print(segment())
