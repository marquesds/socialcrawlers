# My sandbox to get all brazilian cities' latitude and longitude

from bs4 import BeautifulSoup
import requests
from geopy.geocoders.osm import Nominatim
from pymongo import MongoClient


def save(data):
    try:
        client = MongoClient('104.236.213.84', 27017)
        collection = client.locations.brazil
        if data:
            collection.insert(data)
    except Exception as ex:
        print('Error: %s' % ex)


r = requests.get('http://www.notube.com.br/lista-com-todas-cidades-brasil-em-ordem-alfabetica/')
soup = BeautifulSoup(r.text, 'html.parser')

paragraphs = soup.find_all('p')

cities = []
cleaned_cities = []
states_cities = []
dict_cities = {}

for p in paragraphs:
    if '(' in p.text and ')' in p.text:
        cities.append(p.text)

for city in cities:
    cleaned_cities.extend(city.split('\n'))

for city in cleaned_cities:
    if len(city) > 1 and '[' not in city:
        state = city[city.find("(") + 1:city.find(")")]
        city = city.split('(')[0].rstrip()

        states_cities.append(city + ' - ' + state)

geolocator = Nominatim()

for location in states_cities:
    try:
        geo = geolocator.geocode(location, timeout=None)
        city, state = location.strip().rsplit('-', 1)
        locations = {
            'cidade': city.rstrip(),
            'estado': state.rstrip(),
            'latitude': geo.latitude,
            'longitude': geo.longitude
        }
        save(locations)
        print(locations)
    except Exception as ex:
        print('Error: %s' % ex)

# Com dicionário/arquivo json:
# for city in cleaned_cities:
#     if len(city) > 1 and '[' not in city:
#         state = city[city.find("(") + 1:city.find(")")]
#         if state not in dict_cities:
#             dict_cities[state] = []
#             dict_cities[state].append(city.split('(')[0].rstrip())
#         else:
#             dict_cities[state].append(city.split('(')[0].rstrip())
#
# with open('cities.json', 'w') as file:
#     file.write(json.dumps(dict_cities))
