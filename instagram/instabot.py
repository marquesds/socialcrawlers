import logging

from geopy.geocoders.osm import Nominatim
from instagram.bind import InstagramAPIError
from instagram.client import InstagramAPI
from decorators import decorate_all_methods, retry_if_error

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Instagram():
    def __init__(self, access_token, client_secret):
        logger.debug('Instantiating Instagram API')
        self.access_token = access_token
        self.client_secret = client_secret
        self.api = InstagramAPI(access_token=self.access_token, client_secret=self.client_secret)


@decorate_all_methods(retry_if_error)
class User():
    def __init__(self, api, user_name):
        logger.debug('Instantiating User')
        self.api = api
        self.user_name = user_name
        self.user = self.api.user('285217648')
        self.user_id = self.user.id
        self.all_media = [media for media in self.media()]

    def info(self):
        return {
            'id': self.id(),
            'full_name': self.full_name(),
            'username': self.username(),
            'bio': self.bio(),
            'website': self.website(),
            'profile_picture': self.profile_picture()
        }

    def id(self):
        return self.api.user_search(self.user_name)[0].id

    def user(self):
        return self.api.user(self.user_id)

    def full_name(self):
        return self.user.full_name

    def username(self):
        return self.user.username

    def bio(self):
        return self.user.bio

    def website(self):
        return self.user.website

    def profile_picture(self):
        return self.user.profile_picture

    def media(self, next_bkp=None):
        media, next_ = self.api.user_recent_media(user_id=self.user_id, count=10)

        try:
            if next_bkp:
                while next_bkp:
                    next_ = next_bkp
                    new_media, next_bkp = self.api.user_recent_media(with_next_url=next_bkp)
                    media.extend(new_media)
            else:
                while next_:
                    new_media, next_ = self.api.user_recent_media(with_next_url=next_)
                    media.extend(new_media)
        except InstagramAPIError as ex:
            logger.error('You reached Instagram API limit. Waiting...')
            if '429' in str(ex.status_code):
                self.media(next_bkp=next_)

        for m in media:
            yield m

    def follows(self, next_bkp=None):
        follows, next_ = self.api.user_follows(user_id=self.user_id)

        try:
            if next_bkp:
                while next_bkp:
                    next_ = next_bkp
                    more_follows, next_bkp = self.api.user_follows(with_next_url=next_bkp)
                    follows.extend(more_follows)
            else:
                while next_:
                    more_follows, next_ = self.api.user_follows(with_next_url=next_)
                    follows.extend(more_follows)
        except InstagramAPIError as ex:
            logger.error('You reached Instagram API limit. Waiting...')
            if '429' in str(ex.status_code):
                self.follows(next_bkp=next_)

        for user in follows:
            yield user.username

    def followers(self, next_bkp=None):
        followers, next_ = self.api.user_followed_by(user_id=self.user_id)

        try:
            if next_bkp:
                while next_bkp:
                    next_ = next_bkp
                    more_followers, next_bkp = self.api.user_followed_by(with_next_url=next_bkp)
                    followers.extend(more_followers)
            else:
                while next_:
                    more_followers, next_ = self.api.user_followed_by(with_next_url=next_)
                    followers.extend(more_followers)
        except InstagramAPIError as ex:
            logger.error('You reached Instagram API limit. Waiting...')
            if '429' in str(ex.status_code):
                self.followers(next_bkp=next_)

        for user in followers:
            yield user.username

    def total_media(self):
        return self.user.counts['media']

    def total_follows(self):
        return self.user.counts['follows']

    def total_followers(self):
        return self.user.counts['followed_by']

    def search_by_location(self, q, location):
        geolocator = Nominatim()
        location = geolocator.geocode(location)

        return self.api.user_search(q=q, lat=location.latitude, lng=location.longitude, distance=5000, count=500000)

    def all_data(self):
        logger.debug('Getting all user data')
        return {
            'type': 'about',
            'info': self.info(),
            'follows': [follow for follow in self.follows()],
            'followers': [follower for follower in self.followers()],
            'all_media': [media.id for media in self.all_media],
            'total_media': self.total_media(),
            'total_follows': self.total_follows(),
            'total_followers': self.total_followers()
        }

    def __str__(self):
        return 'User: %s' % self.user.username


@decorate_all_methods(retry_if_error)
class Media():
    def __init__(self, api, media):
        logger.debug('Instantiating Media')
        self.api = api
        self.media = media
        self.media_id = self.id()

    def info(self):
        return {
            'type': self.type(),
            'user': self.username(),
            'created_time': self.created_at(),
            'text': self.text(),
            'tags': self.tags(),
            'filter': self.filter(),
            'users_in_photo': [user for user in self.users_in_photo()],
            'location': self.location(),
            'link': self.link()
        }

    def id(self):
        return self.media.id

    def type(self):
        return self.media.type

    def username(self):
        return self.media.user.username

    def created_at(self):
        return self.media.created_time

    def text(self):
        return self.media.caption.text if hasattr(self.media.caption, 'text') else ''

    def filter(self):
        return self.media.filter

    def location(self):
        return str(self.media.location) if hasattr(self.media, 'location') else ''

    def link(self):
        return self.media.link

    def total_likes(self):
        return self.media.like_count

    def who_liked(self):
        for user in self.api.media_likes(self.media_id):
            yield user.username

    def users_in_photo(self):
        for user in self.media.users_in_photo:
            yield user.user.username

    def tags(self):
        tags = []
        for tag in self.media.tags:
            tags.append(tag.name)
        return tags

    def comments(self):
        for comment in self.media.comments:
            yield {
                'id': comment.id,
                'created_at': comment.created_at,
                'text': comment.text,
                'user': comment.user.username
            }

    def search_by_location(self, location):
        geolocator = Nominatim()
        location = geolocator.geocode(location)
        media = self.api.media_search(lat=location.latitude, lng=location.longitude, distance=5000, count=500000)

        return media

    def all_data(self):
        return {
            'type': 'media',
            'media_id': self.media_id,
            'info': self.info(),
            'total_likes': self.total_likes(),
            'who_liked': [who for who in self.who_liked()],
            'tags': self.tags(),
            'comments': [comment for comment in self.comments()]
        }


def __str__(self):
    return 'Media: %s' % self.media_id


@decorate_all_methods(retry_if_error)
class TagSearch():
    def __init__(self, api, tag):
        logger.debug('Instantiating TagSearch')
        self.api = api
        self.tag = tag

    def media(self, next_bkp=None):
        media, next_ = self.api.tag_recent_media(tag_name=self.tag, count=10)

        try:
            if next_bkp:
                while next_bkp:
                    next_ = next_bkp
                    m, next_bkp = self.api.tag_recent_media(tag_name=self.tag, with_next_url=next_bkp)
                    media.extend(m)
            else:
                while next_:
                    m, next_ = self.api.tag_recent_media(tag_name=self.tag, with_next_url=next_)
                    media.extend(m)
        except InstagramAPIError as ex:
            if '429' in ex.status_code:
                logger.error('You reached Instagram API limit. Waiting...')
                self.media(next_bkp=next_)

        for m in media:
            yield m

    def total_media(self):
        return self.api.tag(self.tag).media_count

    def similar_tags(self, next_bkp=None):
        similar_tags, next_ = self.api.tag_search(self.tag)

        try:
            if next_bkp:
                while next_bkp:
                    next_ = next_bkp
                    m, next_bkp = self.api.tag_search(with_next_url=next_bkp)
                    similar_tags.extend(m)
            else:
                while next_:
                    m, next_ = self.api.tag_search(with_next_url=next_)
                    similar_tags.extend(m)
        except InstagramAPIError as ex:
            if '429' in ex.status_code:
                logger.error('You reached Instagram API limit. Waiting...')
                self.similar_tags(next_bkp=next_)

        for tag in similar_tags:
            yield tag.name

    def all_data(self):
        logger.debug('Getting all tag data')
        return {
            'media': [m for m in self.media()],
            'total_media': self.total_media(),
            'similar_tags': [tag for tag in self.similar_tags()]
        }

    def __str__(self):
        return 'Tag: %s' % self.tag
