import logging
import time

from pymongo import MongoClient

from instagram.bind import InstagramAPIError

from instabot import Instagram, User, Media
from settings import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def run(username):
    instagram = Instagram(ACCESS_TOKEN2, CLIENT_SECRET2)
    collection_name = username.replace('.', '').replace('_', '')
    try:
        client = MongoClient(DATABASE_URL, DATABASE_PORT)
        collections = client.instagram
        collection = collections[collection_name]

        # Work only in new connections
        if not collection_name in collections.collection_names():
            user = User(instagram.api, username)
            logger.debug('Saving user')
            with open('user.txt', 'w') as file:
                file.write(str(user.all_data()))
            # collection.insert()
            # print(user.all_data())
            for media in user.media():
                print(media)

            for media in user.all_media:
                try:
                    m = Media(instagram.api, media)
                    logger.debug('Saving media: %s' % m.media_id)
                    collection.insert(m.all_data())
                except InstagramAPIError as ex:
                    if '429' in str(ex.status_code):
                        # More than one hour
                        time.sleep(3700)
                        instagram = Instagram(ACCESS_TOKEN2, CLIENT_SECRET2)
                    logger.error('InstagramAPIError: %s' % str(ex))
                except Exception as ex:
                    logger.error(ex)

            logger.debug('Sending data to database')
        else:
            logger.debug('Collection %s already saved' % collection_name)

    except InstagramAPIError as ex:
        if '429' in str(ex.status_code):
            time.sleep(3700)
        logger.error("Could't save data. Error: %s" % str(ex))
    except Exception as ex:
        logger.error("Could't save data. Error: %s" % str(ex))


if __name__ == '__main__':
    brands = []
    error_brands = []
    try:
        with open('brands.txt') as file:
            for brand in file:
                brands.append(brand.replace('\n', ''))

        for brand in brands:
            try:
                run(brand)
            except Exception as ex:
                logger.error("Could't save brand: %s. Error: %s" % (brand, str(ex)))
                error_brands.append(brand)
    except Exception as ex:
        logger.error('Error: %s' % str(ex))

    """
    with open('result.txt', 'w') as file:
        file.write('Worker has finished.\n')
    """