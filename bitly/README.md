# bitly
Bot que mostra dados relativos a links encurtados pelo bitly

# Instalação das dependências
`pip install -r requirements.txt`

# Configuração
Salve a lista de links que deseja obter informações em `urls` no arquivo `run.py`

# Execução
Para executar o código basta rodar o comando abaixo:
<br>
`python run.py`

# Suporte
Este programa suporta apenas a versão 2 do Python
