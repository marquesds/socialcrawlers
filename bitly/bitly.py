# -*- coding: utf-8 -*-

import datetime

import bitly_api


class Bitly():
    def __init__(self, token):
        self.token = token
        self.api = bitly_api.Connection(access_token=self.token)

    def created_at(self, shortUrl):
        return datetime.datetime.fromtimestamp(
            int(self.api.info(shortUrl=shortUrl)[0]['created_at'])
        )

    def long_url(self, shortUrl):
        return self.api.expand(shortUrl=shortUrl)[0]['long_url']

    def total_clicks(self, shortUrl):
        return self.api.link_clicks(link=shortUrl, units=-1)

    def clicks_by_day(self, shortUrl):
        return self.api.link_clicks(link=shortUrl, unit='day')
