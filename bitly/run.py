import settings
from bitly import Bitly

urls = ['http://bit.ly/1hnwmBU', 'http://bit.ly/1gNSeq5', 'http://bit.ly/1J1eokT', 'http://bit.ly/1Du4qq6']

bitly = Bitly(token=settings.ACCESS_TOKEN)

for url in urls:
    print(bitly.created_at(shortUrl=url))
    print(bitly.long_url(shortUrl=url))
    print(bitly.total_clicks(shortUrl=url))
    print(bitly.clicks_by_day(shortUrl=url))
