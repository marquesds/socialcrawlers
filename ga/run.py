import json
import logging
import datetime

from analytics import GoogleAnalytics
from settings import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)


def google_analytics():
    logger.debug("Creating Google Analytics object")
    ga = GoogleAnalytics('analytics', 'v3', SCOPE, CLIENT_SECRETS_PATH)
    return ga


def dimsmets():
    with open('dimsmets.json') as data:
        logger.debug("Getting all available dimensions and metrics")
        dimsmets = json.load(data)
        return dimsmets


def save_metrics(timestamp):
    metrics = []
    dm = dimsmets()
    ga = google_analytics()
    today = str(datetime.datetime.now())[:10]

    for key in dm:
        metrics.extend(dm[key]['metrics'])

    for metric in metrics:
        try:
            doc = {}
            doc['timestamp'] = timestamp
            doc['name'] = metric
            doc['value'] = ga.get_results(metric, start_date=today,
                                          end_date=today)
            logger.debug("Saving metric: %s" % metric)
            ga.save(doc)
        except:
            logger.error("Could't save result with metric: %s" % metric)


def save_dimensions(timestamp):
    dm = dimsmets()
    ga = google_analytics()
    today = str(datetime.datetime.now())[:10]

    for key in dm:
        for dimension in dm[key]['dimensions']:
            for metric in dm[key]['metrics']:
                try:
                    doc = {}
                    doc['timestamp'] = timestamp
                    doc['name'] = 'dimension_%s_metric_%s' % (dimension, metric)
                    doc['value'] = ga.get_results(metric, dimension, start_date=today,
                                                  end_date=today)
                    logger.debug("Saving dimension: %s and metric: %s" % (dimension, metric))
                    ga.save(doc)
                except:
                    logger.error("Error: Could't save result with dimension: %s and metric: %s" % (dimension, metric))


if __name__ == '__main__':
    logger.debug("Starting bot")
    while True:
        try:
            ts = datetime.datetime.now()
            save_metrics(ts)
            save_dimensions(ts)
        except Exception as ex:
            logger.error("Error:%s" % ex)
