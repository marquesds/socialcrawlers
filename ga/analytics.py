import argparse

from googleapiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
from pymongo import MongoClient
from settings import DATABASE_URL, DATABASE_PORT


class GoogleAnalytics():
    def __init__(self, api_name, api_version, scope, client_secrets_path):
        self.api_name = api_name
        self.api_version = api_version
        self.scope = scope
        self.client_secrets_path = client_secrets_path

        self.service = self.__get_service__()
        self.profile_id = self.__get_first_profile_id__()

    def __get_service__(self):
        """Get a service that communicates to a Google API.

        Args:
          api_name: string The name of the api to connect to.
          api_version: string The api version to connect to.
          scope: A list of strings representing the auth scopes to authorize for the
            connection.
          client_secrets_path: string A path to a valid client secrets file.

        Returns:
          A service that is connected to the specified API.
        """
        # Parse command-line arguments.
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            parents=[tools.argparser])
        flags = parser.parse_args([])

        # Set up a Flow object to be used if we need to authenticate.
        flow = client.flow_from_clientsecrets(
            self.client_secrets_path, scope=self.scope,
            message=tools.message_if_missing(self.client_secrets_path))

        # Prepare credentials, and authorize HTTP object with them.
        # If the credentials don't exist or are invalid run through the native client
        # flow. The Storage object will ensure that if successful the good
        # credentials will get written back to a file.
        storage = file.Storage(self.api_name + '.dat')
        credentials = storage.get()
        if credentials is None or credentials.invalid:
            credentials = tools.run_flow(flow, storage, flags)
        http = credentials.authorize(http=httplib2.Http())

        # Build the service object.
        service = build(self.api_name, self.api_version, http=http)

        return service

    def __get_first_profile_id__(self):
        # Use the Analytics service object to get the first profile id.

        # Get a list of all Google Analytics accounts for the authorized user.
        accounts = self.service.management().accounts().list().execute()

        if accounts.get('items'):
            # Get the first Google Analytics account.
            # TODO: Get account by param
            account = accounts.get('items')[0].get('id')

            # Get a list of all the properties for the first account.
            properties = self.service.management().webproperties().list(
                accountId=account).execute()

            if properties.get('items'):
                # Get the first property id.
                property = properties.get('items')[0].get('id')

                # Get a list of all views (profiles) for the first property.
                profiles = self.service.management().profiles().list(
                    accountId=account,
                    webPropertyId=property).execute()

                if profiles.get('items'):
                    # return the first view (profile) id.
                    return profiles.get('items')[0].get('id')

        return None

    def get_results(self, metrics, dimensions='', start_date='7daysAgo', end_date='today'):
        """Use the Analytics Service Object to query the Core Reporting API
            for the number of sessions within the past N days (7 by default).

        Args:
          metrics: Describe characteristics of your users.
          dimensions: Describes a characteristic of sessions. Default: ''
            All available metrics and dimensions at:
                https://developers.google.com/analytics/devguides/reporting/core/dimsmets#mode=api
            Or:
                dimsmets.json
          start_date: Start date to query. Default: '7daysAgo'
          end_date: End date to query. Default: 'today'

        Returns:
          All specific metrics/dimensions data.
        """
        return self.service.data().ga().get(
            ids='ga:' + self.profile_id,
            start_date=start_date,
            end_date=end_date,
            metrics=metrics,
            dimensions=dimensions).execute()

    def save(self, result):
        try:
            client = MongoClient(DATABASE_URL, DATABASE_PORT)
            collection = client.ga.espn
            if result:
                collection.insert(result)
        except Exception as ex:
            print(ex)
